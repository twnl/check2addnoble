// ==UserScript==
// @name         Check2AddEdel
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://*.tribalwars.nl/game.php?*screen=place&try=confirm*
// @updateURL    https://bitbucket.org/twnl/check2addnoble/raw/c107c751f4668eb00db7014547c01406f17ec812/Check2AddEdel.user.js
// @downloadURL  https://bitbucket.org/twnl/check2addnoble/raw/c107c751f4668eb00db7014547c01406f17ec812/Check2AddEdel.user.js
// @grant        none
// ==/UserScript==

(function() {
    AddEdel();
})();


function AddEdel() {

    var AddEdelButton = document.getElementById('troop_confirm_train');
    var AddEdelViewButton = AddEdelButton.outerHTML;

    if(AddEdelViewButton.search("display: none;") > -1) {
        document.getElementById('troop_confirm_go').focus();
    } else {
        document.getElementById('troop_confirm_train').focus();
    }
}


document.onkeydown = function(e) {
    switch (e.keyCode) {
        case 13:
            AddEdel();
            break;
    }
};